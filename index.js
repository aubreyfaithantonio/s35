const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express()
const port = 3005

// MongoDB connection
mongoose.connect(`mongodb+srv://aubreyfaithantonio:${process.env.MONGODB_PW}@cluster0.qkdjh4x.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", () => console.error('Connection error'))
db.on("open", () => console.log('Connected to MongoDB'))
// MongoDB connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
// Schema END

// Model
const User = mongoose.model('User', userSchema)
// Model END

// [ROUTES]
app.post("/signup", (request, response) => {
	let newUser = new User({
		username: request.body.username,
		password: request.body.password
	})

	newUser.save((error, result) => {
		if(error){
			return console.error(error)
		} 

		return response.status(200).send('User has been created')
	})
})
// [ROUTES END]
app.listen(port, () => console.log(`Server is running at localhost:${port}`))